###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Project: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                                  PV - Remote On/Off valve                                ##
##                                                                                          ##  
##                                                                                          ## 
############################         Version: 1.0             ################################
# Author:  Emilio Asensi
# Date:    18-11-2022
# Version: v1.0
# Changes: 
# 1. Cloned from CMS_PV  



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_Auto",    ARCHIVE=True,           PV_DESC="Operation Mode Auto",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Manual",  ARCHIVE=True,           PV_DESC="Operation Mode Manual",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Forced",  ARCHIVE=True,           PV_DESC="Operation Mode Forced",     PV_ONAM="True",           PV_ZNAM="False")

#Valve physical states
add_digital("Opened",  		  ARCHIVE=True,           PV_DESC="Valve Opened",              PV_ONAM="True",           PV_ZNAM="False")
add_digital("Closed",  		  ARCHIVE=True,           PV_DESC="Valve Closed",              PV_ONAM="True",           PV_ZNAM="False")
add_digital("Solenoid",  	  ARCHIVE=True,           PV_DESC="Solenoid energized",        PV_ONAM="True",           PV_ZNAM="False")
add_analog("ValveColor","INT",                        PV_DESC="BlockIcon valve color")
add_analog("SolenoidColor","INT",                     PV_DESC="BlockIcon solenoid color")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          				  PV_DESC="Inhibit Manual Mode",       PV_ONAM="InhibitManual",  PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           				  PV_DESC="Inhibit Force Mode",        PV_ONAM="InhibitForce",   PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            				  PV_DESC="Inhibit Locking",           PV_ONAM="InhibitLocking", PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("GroupInterlock",          				  PV_DESC="Group Interlock",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("StartInterlock",  ARCHIVE=True,          PV_DESC="Start Interlock",           PV_ONAM="True",           PV_ZNAM="False")
add_digital("StopInterlock",   ARCHIVE=True,          PV_DESC="Stop Interlock",            PV_ONAM="True",           PV_ZNAM="False")

#Block Icon controls
add_digital("EnableBlkOpen",           				  PV_DESC="Enable Block Open Button",  PV_ONAM="True",           PV_ZNAM="False")
add_digital("EnableBlkClose",          				  PV_DESC="Enable Block Close Button", PV_ONAM="True",           PV_ZNAM="False")

#for OPI visualisation
add_digital("EnableAutoBtn",           				  PV_DESC="Enable Auto Button",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("EnableManualBtn",         				  PV_DESC="Enable Manual Button",      PV_ONAM="True",           PV_ZNAM="False")
add_digital("EnableForcedBtn",         				  PV_DESC="Enable Force Button",       PV_ONAM="True",           PV_ZNAM="False")
add_string("InterlockMsg",39,PV_NAME="InterlockMsg",  PV_DESC="Interlock Message")
add_digital("NormallyOpen",            				  PV_DESC="PV is Normally opened")

add_digital("LatchAlarm",                             PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                             PV_DESC="Group Alarm for OPI")


#Alarm signals
add_major_alarm("Opening_TimeOut",     "Opening Time Out",                  PV_ZNAM="NominalState")
add_major_alarm("Closing_TimeOut",     "Closing Time Out",                  PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "IO Error",                          PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",             PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "HW Output Module Error",            PV_ZNAM="NominalState")
add_major_alarm("SSTriggered","SSTriggered",                                PV_ZNAM="NominalState")

#OPI timeouts
add_time("OpeningTime",                PV_DESC="Opening Time")
add_time("ClosingTime",                PV_DESC="Closing Time")

############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                ARCHIVE=True,		PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              ARCHIVE=True,		PV_DESC="CMD: Manual Mode")
add_digital("Cmd_Force",               ARCHIVE=True,		PV_DESC="CMD: Force Mode")
add_digital("Cmd_ManuOpen",            ARCHIVE=True,		PV_DESC="CMD: Manual Open")
add_digital("Cmd_ManuClose",           ARCHIVE=True,		PV_DESC="CMD: Manual Close")
add_digital("Cmd_ForceOpen",           ARCHIVE=True,		PV_DESC="CMD: Force Open")
add_digital("Cmd_ForceClose",          ARCHIVE=True,		PV_DESC="CMD: Force Close")

add_digital("Cmd_AckAlarm",            ARCHIVE=True,		PV_DESC="CMD: Acknowledge Alarm")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
